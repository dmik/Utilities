#!/bin/sh

echo "This will delete ALL files in the current directory (`pwd`) except those located in .svn dirs."
echo "Press Y and ENTER to proceed or anything else to abort."
read ANSWER

if [ "$ANSWER" = "Y" ] ; then 
    find . -type f ! -path "*/.svn/*" -exec rm -f "{}" \;
fi
