#!/bin/sh

: ${SVN:=svn}

echo '
You are about to clean up the current directory:

  '`pwd`'

which is assumed to be an SVN repository.

Please specify the action you want to perform:

  R - reset local modifications to all versioned files.
  N - delete all non-versioned files excluding ignored.
  I - delete all non-versioned files which are ignored.
  C - do N and I together.
  A - do ALL of the above (equivalent to a clean check out).

Any other input will abort the command.'

read ANSWER

case "$ANSWER" in
R)
    $SVN revert -R .
    ;;
N)
    $SVN status | grep '^?' | awk '{print $2}' | xargs rm -rfv
    ;;
I)
    $SVN status --no-ignore | grep '^I' | awk '{print $2}' | xargs rm -rfv
    ;;
C)
    $SVN status --no-ignore | grep '^[?I]' | awk '{print $2}' | xargs rm -rfv
    ;;
A)
    $SVN revert -R .
    if test $? = 0 ; then
        $SVN status --no-ignore | grep '^[?I]' | awk '{print $2}' | xargs rm -rfv
    fi
    ;;
*)
    echo "Command aborted."
    ;;
esac
