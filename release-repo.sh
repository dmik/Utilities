#!/bin/sh

#
# release-repo.sh
#
# Synopsis
# --------
#
# Exports a given commit of a main repository to a separate release repository
# by squashing all commits together in a single commit which effectively wipes
# out all commit history.
#
# Author: Dmitriy Kuminov (coding@dmik.org).
#
# Installation
# ------------
#
# This script must be stored in the root directory of the main repository which
# is about to be exported. The script is configured using two configuration
# files stored in the same directory: `release-repo.config.sh` containing
# per-project settings, and `release-repo.config.local.sh` containing local
# machine-specific settings (optional).
#
# To keep the configuration consistent with the project, both the script and its
# per-project `.config.sh` file should be versioned within the main repository.
# The machine-specific `.config.local.sh` file is not intended to be versioned
# and is usually added to project's `.gitignore`.
#
# Configuration
# -------------
#
# The script is configured using the configuration variables documented in the
# section called 'Configuration variables' below. However, you should never
# change these variables in the script file itself. Instead, create a file named
# `release-repo.config.sh` in the same directory and put all your
# project-specific configuration variables there, to avoid losing them when
# updating the script to a newer version. Optionally, you may also create a file
# named `release-repo.config.local.sh` and put there variables which are
# specific to your local setup (local paths etc.). Note that `.config.sh` is
# read after assigning the default values from 'Configuration variables'
# section, but before `.config.local.sh` (which is read last).
#
# Please refer to 'Configuration variables' for a full list of configuration
# variables and their meaning.
#
# Note that the script as well as its configuration files are never exported to
# the release repository as they may reveal the internal development details
# whhereas the release repository is intended to to hide them.
#
# Usage
# -----
#
# release-repo.sh [COMMIT [RELEAE_TAG]]
#
# When COMMIT is not given, the latest version-like tag (a tag starting with `v`
# and usually containing numbers and dots) is used as an export point. The new,
# squashed commit in the release repository will be tagged using the same tag.
#
# When COMMIT is given, it should specify a valid commit object (e.g. an
# existing tag or a full commit hash) to use as an export point. If COMMIT is
# not a version-like tag, it is also necessary to specify RELEASE_TAG that will
# be used as a tag for the resulting squashed commit.
#
# Currently, only the interactive mode of operation is supported.
#
# Change Log
# ----------
#
# v1 (2023-07-14)
# - Initial release.
#

SCRIPT_VERSION="1"
SCRIPT_PATH=`realpath "$0"`
SCRIPT_NAME="${SCRIPT_PATH##*/}"
SCRIPT_DIR="${SCRIPT_PATH%/*}"

# ------------------------------------------------------------------------------
# Configuration variables
#
# NOTE: Copy this section to release-repo.config[.local].sh for changing!
# ------------------------------------------------------------------------------

# Origin URL of the release repo (required).

#RELEASE_REPO_URL="https://origin/repo.git"

# Where to store a local copy of the release repo (required).

#RELEASE_REPO_DIR="/local/path/to/repo"

# Name of the branch in the main repo to be exported (required).

RELEASE_BRANCH="main"

# Space-separated list of files and directories to exclude when exporting (must
# be relative to the root of the main repo). May contain wildcard characters `?`
# and `*` (`git rm`` style). Spaces and special characters in file names may be
# represented using `\xNN` notation (where NN is a character hex code).

#EXCLUDE_FILES=

# Non-empty for local mode. In local mode, the release repository is linked with
# the local copy of the main repository as upstream instead of its remote orign.
# Useful for releasing in test mode without actually pushing the main repo
# changes to the remote.

LOCAL_MODE=1

# ------------------------------------------------------------------------------
# Globals
# ------------------------------------------------------------------------------

SCRIPT_CONFIG="${SCRIPT_PATH%.sh}.config.sh"
SCRIPT_LOCAL_CONFIG="${SCRIPT_PATH%.sh}.config.local.sh"

[ -e "$SCRIPT_CONFIG" ] && source "$SCRIPT_CONFIG"
[ -e "$SCRIPT_LOCAL_CONFIG" ] && source "$SCRIPT_LOCAL_CONFIG"

if [ -t 1 ] && [ -n "$TERM" ] && [ "$TERM" != "dumb" ]; then
  HAS_ANSI=1
fi

# ANSI color & style variables
A_RED="${HAS_ANSI:+\033[0;31m}"
A_GREEN="${HAS_ANSI:+\033[0;32m}"
A_YELLOW="${HAS_ANSI:+\033[0;33m}"
A_BLUE="${HAS_ANSI:+\033[0;34m}"
A_MAGNETA="${HAS_ANSI:+\033[0;35m}"
A_CYAN="${HAS_ANSI:+\033[0;36m}"
A_WHITE="${HAS_ANSI:+\033[0;37m}"
A_BOLD="${HAS_ANSI:+\033[1m}"
A_NC="${HAS_ANSI:+\033[0m}" # No color & style

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

str_replace ()
{
  local str="$1"
  local r=
  if [ -n "$2" ]; then
    local sp=
    local ss=
    local to_alt=
    while [ -n "$str" ]; do
      sp="${str%%"$2"*}"
      [ "$sp" = "$str" ] && break
      ss="${str#*"$2"}"
      if [ -n "$4" ]; then
        if [ -z "$to_alt" ]; then
          to="$3"
          to_alt=1
        else
          to="$4"
          to_alt=
        fi
      else
        to="$3"
      fi
      r="$r$sp$to"
      str="$ss"
    done
  fi
  printf "$r$str"
}

stylize_text ()
{
  local r=`str_replace "$1" "*" "$A_BOLD" "$A_NC"`
  r=`str_replace "$r" "'" "$A_WHITE'" "'$A_NC"`
  printf "$r"
}

msg_prefix ()
{
  local prefix="$1"
  shift
  while [ -n "$1" ]; do
    printf "$prefix`stylize_text "$1"`\n"
    shift
  done
}

info ()
{
  msg_prefix "${A_GREEN}INFO:${A_NC} " "$@"
}

advice ()
{
  msg_prefix "${A_CYAN}ADVICE:${A_NC} " "$@"
}

warn ()
{
  msg_prefix "${A_YELLOW}WARNING:${A_NC} " "$@"
}

err ()
{
  msg_prefix "${A_RED}ERROR:${A_NC} " "$@"
  exit 1
}

abort ()
{
  info "Aborting."
  exit 1
}

run ()
{
  info "Running [$*]..."
  "$@" || err "Command failed with status $?."
}

ask_yn ()
{
  while [ -n "$1" ]; do
    if [ -n "$2" ]; then
      tmp="\n"
    else
      tmp=" "
    fi
    printf "${A_MAGNETA}QUESTION:${A_NC} `stylize_text "$1$tmp"`"
    shift
  done
  local r
  read -r r
  [ "$r" = "Y" -o "$r" = "y" ] && return 0
  return 1
}

check_var ()
{
  eval [ -z "\$$1" ] && err "Variable $1 is not set."
}

# ------------------------------------------------------------------------------
# Main
# ------------------------------------------------------------------------------

if [ -n "$1" ]; then
  export_commit="$1"
  export_tag="$2"
  if [ -z "$export_tag" ]; then
    case "$export_commit" in
      v[0-9]*) export_tag="$export_commit" ;;
      *) err "Commit '$export_commit' is not a 'vNNN' tag, specify a tag name."
    esac
  fi
fi

check_var "RELEASE_REPO_DIR"
check_var "RELEASE_REPO_URL"
check_var "RELEASE_BRANCH"

source_repo_dir=`git -C "$SCRIPT_DIR" rev-parse --show-toplevel 2>/dev/null` || \
  err "This script ('$SCRIPT_PATH') is not inside a git repository."
source_repo_url=`git remote get-url origin` || \
  err "Repository in '$source_repo_dir' doesn't have origin remote."

[ -n "$LOCAL_MODE" ] && info "Running in *local* mode."

info "Source repository: '$source_repo_dir' ($source_repo_url)"

if [ -z "$export_commit" ]; then
  for tmp in `git tag --sort=-v:refname`; do
    export_tag="$tmp"
    break
  done
  [ -n "$export_tag" ] || err "No tags found."

  case "$export_tag" in
    v[0-9]*) ;;
    *) err "Tag '$export_tag' doesn't match 'vNNN' format." ;;
  esac
  export_commit="$export_tag"
fi

tmp=`git log --format=%s -n 1 $export_commit`
info "Source commit: '$export_commit' (${tmp%.})"
info "Target tag: '$export_tag'"

target_repo_dir="$RELEASE_REPO_DIR"
case "$target_repo_dir" in
  /*) ;;
  \~/*) target_repo_dir="$HOME/${RELEASE_REPO_DIR#\~/}" ;;
  *) target_repo_dir="$SCRIPT_DIR/$RELEASE_REPO_DIR" ;;
esac

if [ -d "$target_repo_dir" ]; then
  target_repo_dir=`realpath -q "$target_repo_dir"`
  [ "`git -C "$target_repo_dir" rev-parse --show-toplevel 2>/dev/null`" = "$target_repo_dir" ] || \
    err "'$target_repo_dir' is not a root directory of a git repository."
  run cd "$target_repo_dir"
  [ "`git remote get-url origin`" = "$RELEASE_REPO_URL" ] || \
    err "Origin remote of repository '$target_repo_dir' is not '$RELEASE_REPO_URL'."
else
  info "Directory '$target_repo_dir' does not exist."
  ask_yn "Create it and clone '$RELEASE_REPO_URL' there?" || abort
  run mkdir -p "$target_repo_dir"
  target_repo_dir=`realpath -q "$target_repo_dir"`
  run cd "$target_repo_dir"
  run git clone "$RELEASE_REPO_URL" .
fi

info "Target repository: '$target_repo_dir' ($RELEASE_REPO_URL)"

tmp=`git remote get-url upstream 2>/dev/null`
if [ -z "$tmp" ]; then
  if [ -n "$LOCAL_MODE" ]; then
    run git remote add upstream "$source_repo_dir"
  else
    run git remote add upstream "$source_repo_url"
  fi
else
  if [ "$tmp" = "$source_repo_url" ]; then
    if [ -n "$LOCAL_MODE" ]; then
      run git remote set-url upstream "$source_repo_dir"
    fi
  elif [ "$tmp" = "$source_repo_dir" ]; then
    if [ -z "$LOCAL_MODE" ]; then
      run git remote set-url upstream "$source_repo_url"
    fi
  else
    err "Upstream remote '$tmp' of repository '$target_repo_dir' is not '$source_repo_url' or '$source_repo_dir'."
  fi
fi

[ -z "`git status --porcelain`" ] || \
  err "Taget repository is not clean."

for tag in `git tag --sort=-v:refname`; do
  [ "$tag" != "$export_commit" ] || err "Tag '$export_commit' already exists in the target repository."
done

ask_yn "Proceed with the release?" || abort

tmp=`git rev-list --count HEAD 2>/dev/null`
[ -z "$tmp" ] && tmp=0
if [ "$tmp" -eq 0 ]; then
  info "Target repository has no commits."
  run git fetch upstream --depth=1 --no-tags "$export_commit"
  run git reset --hard FETCH_HEAD
  run git update-ref -d HEAD
else
  info "Target repository has $tmp commit(s)."
  run git fetch upstream --depth=1 --no-tags "$export_commit"
  run git reset --hard FETCH_HEAD
  run git reset --soft ORIG_HEAD
fi

info "Removing excluded files..."

for f in "$SCRIPT_NAME" "${SCRIPT_CONFIG##*/}" "${SCRIPT_LOCAL_CONFIG##*/}" $EXCLUDE_FILES; do
  f=`printf "$f"` # to make sure \xNN are processed
  run git rm -rfq --ignore-unmatch "$target_repo_dir/$f"
done

info "Committing & tagging..."

run git commit -m "Release $export_tag."
run git tag "$export_tag"

info "Done releasing ${latset_tag}."
advice "Check the target repository manually before proceeding any further."

if ask_yn \
  "Are you ready to push the changes to origin?" && \
  ask_yn "*This cannot be easily undone. Are you sure?*"; then
    run git push
    run git push --tags
else
  info "Did not push."
  advice "Run 'git push ; git push --tags' manually when ready."
fi

info "All done."

exit
