/**
 * PMDF REXX Command 
 *
 * Searches for a .sym file for the process EXE and for each loaded DLL
 * in the same directory where the EXE/DLL resides and activates it if
 * found.
 *
 * Usage: %pmdf-loadallsym
 */
 
/* get basic process info */
exe_hmte = ''
address df 'CMD' 'proc_info' '.i'
parse var proc_info.3 'MTE' 'handle='exe_hmte .

exe_path = ''
address df 'CMD' 'out' '.lmx'
do i = 1 to out.0
    parse var out.i 'hmte='hmte . . fn
    if (hmte == exe_hmte) then do
        exe_path = fn
        leave
    end
end

/* get list of all DLLs */
address df 'CMD' 'out' '.lml'

/* add process EXE to the list */
i = out.0 + 1
out.i = 'a' 'b' 'c' exe_path
out.0 = i

/* search for .SYM for each EXE/DLL */
do i = 1 to out.0
    parse var out.i . . . fn
    if (fn == '') then iterate
    if (stream(fn, 'C', 'QUERY EXISTS') == '') then do
        /* the DLL is not found on the test PC, search it in
         * (BEGIN)LIBPATH */
        config_sys = SysBootDrive()'\CONFIG.SYS'
        do while lines(config_sys)
            line = strip(linein(config_sys))
            if (left(line, 8) == 'LIBPATH=') then do
                search_path = substr(line, 9)
                leave
            end
        end
        call lineout config_sys
        search_path = SysQueryExtLibPath('B')';'search_path';'SysQueryExtLibPath('E')
        if (search_path \= '') then do
            file = filespec('N', fn)
            old_libpath = value('PMDF_LIBPATH',, 'OS2ENVIRONMENT')
            call value 'PMDF_LIBPATH', search_path';'old_libpath, 'OS2ENVIRONMENT'
            real_file = SysSearchPath('PMDF_LIBPATH', file)
            call value 'PMDF_LIBPATH', old_libpath, 'OS2ENVIRONMENT'
            if (real_file \= '') then
                fn = real_file
        end
    end
    j = lastpos('.', fn)
    if (j == 0) then do
        sym = stream(fn'.sym', 'C', 'QUERY EXISTS')
    end
    else do
        base = left(fn, j - 1)
        sym = stream(base'.sym', 'C', 'QUERY EXISTS')
        if (sym == '') then
            sym = stream(fn'.sym', 'C', 'QUERY EXISTS')
    end
    if (sym \== '') then do
        /* .SYM found */
        address df 'CMD' 'out2' 'wa' sym
        do j = 1 to out2.0
            if (out2.j \= '' & left(out2.j, 1) \= '#') then say out2.j
        end
    end
end
