/**
 * PMDF REXX Command 
 *
 * Searches for an EXE/DLL module owning the given address.
 *
 * Usage: %pmdf-findmod <laddr>
 */

numeric digits 12

parse arg laddr

laddr_d = x2d(laddr)

/* get list of all modules */
address df 'CMD' 'out' '.lm'

/* search for the given address */
do i = 1 to out.0
    out.i = strip(strip(out.i),,'0d'x)
    parse var out.i 'hmte='hmte .
    if (hmte == '') then iterate
    /* say 'HMTE('hmte')' */
    /* get list of all objects of this module */
    address df 'CMD' 'out2' '.lmo 'hmte
    is_obj = 0
    do j = 1 to out2.0
        out2.j = strip(strip(out2.j),,'0d'x)
        if (\is_obj) then do
            if (left(out2.j, 3) == 'obj') then do
                is_obj = 1
                iterate
            end
        end
        else do
            if (left(out2.j, 3) == 'obj') then iterate
            parse var out2.j vobj vsize vbase .
            /* say 'VV('vsize')('vbase')' */
            if (vsize == '' | vbase == '') then iterate
            vsize_d = x2d(vsize)
            vbase_d = x2d(vbase)
            if (laddr_d >= vbase_d & laddr_d < vbase_d + vsize_d) then do
                say 'Found module for laddr 'laddr' ('vobj':'d2x(laddr_d-vbase_d, 8)'):'
                say out.i
                say out2.j
                say
                exit
            end
        end
    end
end

say 'Could not find module for laddr 'laddr'.'
say
