#!/bin/sh

. configure-env.sh

SRCDIR="$(readlink -f $0)"
SRCDIR=${SRCDIR%/*}
SRCDIR=${SRCDIR%-build}

${SRCDIR}/configure --prefix=/@unixroot/usr/local
