#!/bin/sh

export LANG=en_US

export SHELL=sh            # general shell
export EMXSHELL=$SHELL     # LIBC shell
export CONFIG_SHELL=$SHELL # configure shell
export MAKESHELL=$SHELL    # make shell
export EXECSHELL=$SHELL    # perl shell

export PATH=`echo $PATH | sed -e 's@\\\\@/@g'`
export PATH_SEPARATOR=';'
export ac_executable_extensions='.exe'

# Linker settings (normally set by configure)

#LDFLAGS='-Zomf -Zmap -Zargs-wild -Zhigh-mem -Zargs-resp'

# Optimizer settings (normally set by configure)

#CFLAGS='-O3 -march=pentium -mtune=pentium4'
#CXXFLAGS='-O3 -march=pentium -mtune=pentium4'

# Other stuff (normally done by configure)

#function test
#{
#  if [  "$1" = "-x" ] ; then
#    shift
#    if [ -f "$1" ] ; then return ; fi
#    if type $1.cmd 1>nul 2>&1  ; then  return ; fi
#    if type $1.exe 1>nul 2>&1  ; then  return ; fi
#    x=`type $* 1>nul 2>&1|  sed -e 's@^.* @@' -e 's@\\\\@/@g' -e 's@\\.$@@'`
#    if [ -n "$x" ] ; then
#      grep '^\(#!\|[ \t]*extproc\)' "$x">nul 2>&1
#    else
#      return 1
#    fi
#  else
#    builtin test "$@"
#  fi ;\
#}
