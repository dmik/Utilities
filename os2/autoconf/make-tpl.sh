#!/bin/sh

. configure-env.sh

DESTDIR="$(readlink -f $0)"
DESTDIR=${DESTDIR%/*}
DESTDIR=${DESTDIR%-build}-install
make install DESTDIR="${DESTDIR}"
