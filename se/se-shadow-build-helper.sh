#!/bin/sh

#
# This script is intended to be sourced from an env.sh script that is run by the SE
# utility. Its purpose is to handle common commands done when auto-making, configuring
# and building an autotools-based project in a shadow (dedicated) build directory.
#
# An example env.sh file:
#
# -----------------------------------------------------------------------------
#
# #!/bin/sh
#
# # Define common vars here.
#
# myautoconf()
# {
#     # This command runs autotools to prepare the configure script etc. Must be
#     # executed from the root of the source tree.
#
#     # Call bootstrap.sh, autogen.sh, etc. here.
#     # $ARGS is set to function arguments (if any).
#     # $CUR_DIR is the full path to the current directory.
# }
#
# myconfigure()
# {
#     # This configures the build using the configure script. Must be executed
#     # from the root of the build directory.
#
#     # Call configure with necessary flags here (see a snippet below).
#     # $ARGS is set to myconfigure arguments (if any).
#     # $BUILD_ROOT is the full path to the build directory.
#     # $BUILD_TYPE is the build type (release, debug etc).
#     # $SOURCE_ROOT is the source directory.
#
#     case "$BUILD_TYPE" in
#     "release")
#         run "$SOURCE_ROOT/configure" \
#             --prefix=/@unixroot/usr/local \
#             "$ARGS"
#         ;;
#     "debug")
#         export CFLAGS="$CFLAGS -g"
#         export CXXFLAGS="$CXXFLAGS -g"
#         export LDFLAGS="$LDFLAGS -g"
#         run "$SOURCE_ROOT/configure" \
#             --prefix=/@unixroot/usr/local \
#             --enable-debug \
#             "$ARGS"
#         ;;
#     *)
#         die "Unknown build type $BUILD_TYPE."
#         ;;
#     esac
# }
#
# mymake()
# {
#   Builds the project. Must be executed from the root of the build directory.
#
#   # Call make here.
#   # All vars are the same as for myconfigure.
# }
#
# myinstall()
# {
#   Installs the build. Must be executed from the root of the build directory.
#
#   # Call make install here.
#   # All vars are the same as for mymake.
# }
#
# mypackage()
# {
#   Packages the build. Must be executed from the root of the build directory.
#
#   # Call tar/zip etc. here.
#   # All vars are the same as for mymake.
# }
#
# myfoobar
# {
#   # A custom function. No special behavior is excpected.
#
#   # $ARGS is set to function arguments (if any).
#   # $CUR_DIR is the full path to the current directory.
# }
#
# . se-shadow-build-helper.sh
#
# -----------------------------------------------------------------------------
#

run()
{
    "$@"
    local rc=$?
    if test $rc != 0 ; then
        echo "ERROR: The following command failed with error code $rc:"
        echo $@
        exit $rc
    fi
}

die()
{
    echo "ERROR: $1"
    exit 1
}

check_files()
{
    for f in $*
    do
        case "$f" in
        */) test -d "$f" || die "Directory '$f' is missing." ;;
        *) test -f "$f" || die "File '$f' is missing." ;;
        esac
    done
}

if test "x$SE_CMD_RUNNING" != "x" ; then
    # Running under SE.
    CUR_DIR="$SE_CMD_CWD"
    CMD="${SE_CMD_ARGS%% *}"
    ARGS="${SE_CMD_ARGS#$CMD}"
else
    # Running under SH directly.
    CUR_DIR=`pwd`
    CMD="${@%% *}"
    ARGS="${@#$CMD}"
fi

case "$CMD" in

myautoconf)

    myautoconf
    exit $?
    ;;

myconfigure|mymake|myinstall|myrun|mypackage)

    case "${CUR_DIR}" in
    */*-build*/*|*/*-build*)
        SOURCE_ROOT=${CUR_DIR%%-build*}
        BUILD_TYPE=${CUR_DIR##*/*-build}
        REL_DIR=${BUILD_TYPE#*/}
        if [ "$REL_DIR" = "$BUILD_TYPE" ] ; then
            SOURCE_CUR="$SOURCE_ROOT"
            REL_DIR=
        else
            SOURCE_CUR="$SOURCE_ROOT/$REL_DIR"
        fi
        test -d "$SOURCE_CUR" || die "'$SOURCE_CUR' must be a directory."
        BUILD_TYPE=${BUILD_TYPE%%/*}
        if [ -z "$BUILD_TYPE" ] ; then
            BUILD_ROOT=$SOURCE_ROOT-build
            BUILD_TYPE=release
        else
            BUILD_ROOT=${SOURCE_ROOT}-build${BUILD_TYPE}
        fi
        BUILD_TYPE=${BUILD_TYPE#-}
        ;;
    *)
        die "Current directory '$CUR_DIR' doesn't match the 'PATH/PROJECT-build*' pattern."
        ;;
    esac

    "$CMD"

    exit $?
    ;;

-?|-h|--help)
    echo "Special commands:"
    echo " myautoconf - autoconf in all needed places in current source dirr"
    echo " myconfigure - configure current build dir"
    echo " mymake - make everything in current build dir"
    echo " myinstall - install everything in current build dir"
    echo " myrun - run program in current build dir"
    echo " mypackage - package everything and build the packages"
    exit 1
    ;;

*)
    $CMD $ARGS
    exit $?
    ;;

esac
