#!/usr/bin/env python3

#
# Executes a command on a remote host via SSH by translating local paths to remote paths on input
# and optionally translating them back on output.
#
# The script assumes that the local path represents a file system of the remote host mounted with
# one of the network protocols (FTP, SMB, WEBDAV, etc).
#
# Author: Dmitriy Kuminov, coding@dmik.org
#

import os, os.path, sys, argparse, re, subprocess

#
# globals
#

g_fschars = '\\w=+!@#$%^&()-_;~,.{}[\\]'

#
# options
#

opts_parser = argparse.ArgumentParser ()
opts_parser.add_argument ('-c', '--connection', action = 'store', dest = 'ssh_conn', default = os.environ.get ('SSH_EXEC_CONNECTION'))
opts_parser.add_argument ('-m', '--mapping', action = 'append', dest = 'mappings', default = [])
opts_parser.add_argument ('-o', '--map-output', action = 'store_true', dest = 'map_output')
opts_parser.add_argument ('-M', '--make', action = 'store_true', dest = 'make')
opts_parser.add_argument ('-s', '--source-tree', action = 'store', dest = 'source_tree')
opts_parser.add_argument ('-b', '--build-tree', action = 'store', dest = 'build_tree')
opts_parser.add_argument ('-f', '--makefile', action = 'store', dest = 'makefile', default = 'Makefile')
opts_parser.add_argument ('-q', '--quiet', action = 'store_true', dest = 'quiet')
opts_parser.add_argument ('-L', '--local', action = 'store_true', dest = 'local')
opts_parser.add_argument ('-D', '--dos', action = 'store_true', dest = 'dos')
opts_parser.add_argument ('args', nargs = argparse.REMAINDER, metavar = '...', help = "command and its arguments")

opts = opts_parser.parse_args ()
args = opts.args

if len (args) < 1:
    opts_parser.error ('Must specify a command to execute on a remote host.')

if opts.ssh_conn is None:
    opts_parser.error ('Must specify ssh connection parameters with `-c` or in SSH_EXEC_CONNECTION.')

opts.ssh_conn = opts.ssh_conn.split ()

mappings = []

for m in opts.mappings + os.environ.get ('SSH_EXEC_MAPPINGS').split (';'):
    local, remote = m.split ('=');
    real = os.path.realpath(os.path.expanduser(local))
    if real == local:
      real = None
    if opts.dos:
      mappings.append ([[local + '\\1', re.compile ('(?<![\\w/])%s((/[%s]*)*)(?=\\s|\\W)' % (re.escape (local), g_fschars))],
                        [remote + '\\1', re.compile ('(?<![\\w/\\\\])%s(([/\\\\][%s]*)*)(?=\\s|\\W)' % (re.escape (remote), g_fschars))],
                        [real + '\\1', re.compile ('(?<![\\w/])%s((/[%s]*)*)(?=\\s|\\W)' % (re.escape (real), g_fschars)) if real else None]])
    else:
      mappings.append ([[local + '\\1', re.compile ('(?<![\\w/])%s((/[%s]*)*)(?=\\s|\\W)' % (re.escape (local), g_fschars))],
                        [remote + '\\1', re.compile ('(?<![\\w/])%s((/[%s]*)*)(?=\\s|\\W)' % (re.escape (remote), g_fschars))],
                        [real + '\\1', re.compile ('(?<![\\w/])%s((/[%s]*)*)(?=\\s|\\W)' % (re.escape (real), g_fschars)) if real else None]])

if len (mappings) == 0:
    opts_parser.error ('Must specify mappings with `-m` or in SSH_EXEC_MAPPINGS.')

if opts.source_tree or opts.build_tree:
    opts.make = True

#
# main
#

cwd = os.getcwd ()

if not opts.make:

    build_cwd = cwd

else:

    if not opts.source_tree or opts.source_tree == '':
        opts.source_tree = cwd
    if not opts.build_tree or opts.build_tree == '':
        opts.build_tree = opts.source_tree

    if not cwd.startswith (opts.source_tree):
        print ('Current directory `%s` does not start with `%s`. Fix your `-s` option.' % (cwd, opts.source_tree))
        exit (1)

    source_base = cwd [len (opts.source_tree):].strip (os.sep)

    build_cwd = None

    tmp = source_base

    while True:
        if os.path.isfile (os.path.join (opts.build_tree, tmp, opts.makefile)):
            build_cwd = os.path.join (opts.build_tree, tmp).rstrip (os.sep)
            break
        if (tmp == ''):
            break
        tmp = os.path.dirname (tmp)

    if build_cwd == None:
        print ('Could not find `%s` while traversing from `%s` up to `%s`.' % \
               (opts.makefile, os.path.join (opts.build_tree, source_base), opts.build_tree))
        exit (1)

def map_to_remote (s, mapped = None):
    s2 = s
    for m in mappings:
        s2 = m [0] [1].sub (m [1] [0], s2)
        if m [2] [0]:
          s2 = m [2] [1].sub (m [1] [0], s2)
    if (mapped and mapped [0] == False and s != s2):
        mapped [0] = True
    return s2

def map_to_local (s):
    for m in mappings:
        s = m [1] [1].sub (m [0] [0], s)
    return s

def escape_arg (s):
    if ' ' in s:
        return '\'%s\'' % s
    return s

mapped = [ False ]

build_cwd_remote = map_to_remote (build_cwd, mapped)

# Reset cwd to home path if we can't map it
if build_cwd_remote == build_cwd:
    build_cwd_remote = '.'

args_remote = [ escape_arg (map_to_remote (a, mapped)) for a in args ]

# Now we check if local mode is allowed and we have a mapping of either the
# current directory or any argument. If we have a mapping, we run the command
# via SSH, otherwise we run it locally as is.

if (not opts.local or mapped [0]):
    if opts.dos:
        # CMD-like shell is on the remote side
        build_cwd_remote = build_cwd_remote.replace('/', '\\')
        ssh_cmd = ['ssh'] + opts.ssh_conn + ['"cd ' + build_cwd_remote + " && " + " ".join (args_remote) + '"']
    else:
        # Otherwise assume Posix shell
        ssh_cmd = ['ssh'] + opts.ssh_conn + ['sh', '-c', '"cd ' + build_cwd_remote + ' && ' + ' '.join (args_remote) + '"']
else:
    # No need to map output when running locally.
    opts.map_output = False
    ssh_cmd = ['sh', '-c', 'cd ' + build_cwd + ' && ' + ' '.join ([ escape_arg (a) for a in args ])]

if not opts.quiet:
    sys.stdout.write ('[Running%s `%s`]\n' % (' (mapped)' if mapped [0] else '', ' '.join (ssh_cmd)))
    sys.stdout.flush ()

if opts.map_output:

    # NOTE: Code to read and filter stderr and stdout separately is borrowed from:
    # https://stackoverflow.com/questions/31833897/python-read-from-subprocess-stdout-and-stderr-separately-while-preserving-order
    #
    # NOTE 2: Signal handling code is borrowed from https://stackoverflow.com/questions/13593223/making-sure-a-python-script-with-subprocesses-dies-on-sigint

    from threading import Thread
    from queue import Queue
    import signal

    def reader (pipe, queue):
        try:
            with pipe:
                for line in iter (pipe.readline, ''):
                    queue.put ((pipe, line))
        finally:
            queue.put (None)

    proc = subprocess.Popen (ssh_cmd, text = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, bufsize = 1)
    q = Queue ()

    def handle_signal (signum, frame):
        if proc.poll () is None:
            proc.send_signal (signum)
        else:
            signal.signal (signal.SIGINT, signal.SIG_DFL)
            signal.signal (signal.SIGTERM, signal.SIG_DFL)
            signal.raise_signal (signum)

    signal.signal (signal.SIGINT, handle_signal)
    signal.signal (signal.SIGTERM, handle_signal)

    Thread (target = reader, args = [proc.stdout, q]).start()
    Thread (target = reader, args = [proc.stderr, q]).start()

    for _ in range (2):
        for source, line in iter (q.get, None):
            if source == proc.stdout:
                sys.stdout.write (map_to_local (line))
                sys.stdout.flush ()
            else:
                sys.stderr.write (map_to_local (line))
                sys.stderr.flush ()

    exit (proc.wait ())

else:

    exit (subprocess.call (ssh_cmd, stdout = 1, stderr = 2))
