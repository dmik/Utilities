#!/usr/bin/env python3

#
# Simple tee-like logger but also captures stderr in addition to stdout.
#
# Author: Dmitriy Kuminov, coding@dmik.org
#

import os, os.path, sys, argparse, re, subprocess, datetime, time

#
# globals
#


#
# options
#

opts_parser = argparse.ArgumentParser (formatter_class = argparse.ArgumentDefaultsHelpFormatter)
opts_parser.add_argument ('-f', '--logfile', action = 'store', dest = 'logfile', default='@#.log',
  help = 'log file name (@ replaced with command name, # - with unique timestamp)')
opts_parser.add_argument ('args', nargs = argparse.REMAINDER, metavar = '...',
  help = 'command and its agruments')

opts = opts_parser.parse_args ()
args = opts.args

if len (args) < 1:
  opts_parser.error ('Must specify a command to execute for logging.')

timestamp = '-%s-%s' % (datetime.datetime.now ().strftime ('%Y%m%d%H%M%S'), str (os.getpid()).zfill (5))
prog = os.path.basename (args [0])
logfile = opts.logfile.replace ('@', prog).replace ('#', timestamp)

suff = 0
while (os.path.isfile (logfile)):
  suff += 1
  logfile = opts.logfile.replace ('@', prog).replace ('#', '%s-%s' % (timestamp, suff))

try:
  log = open (logfile, 'w')
except Exception as e:
  sys.stderr.write ('ERROR: Failed to open log file: %s\n' % str (e))
  exit (255)

def writeLog (str):
    log.write (str)
    log.flush ()

def writeStdOut (str):
    sys.stdout.write (str)
    sys.stdout.flush ()
    writeLog (str)

def writeStdErr (str):
    sys.stderr.write (str)
    sys.stderr.flush ()
    writeLog (str)

#
# main
#

# NOTE: Code to read and filter stderr and stdout separately is borrowed from:
# https://stackoverflow.com/questions/31833897/python-read-from-subprocess-stdout-and-stderr-separately-while-preserving-order

from threading import Thread
from queue import Queue

def reader (pipe, queue):
  try:
    with pipe:
      for line in iter (pipe.readline, ''):
        queue.put ((pipe, line))
  finally:
    queue.put (None)

writeStdErr ('[%s - Starting \'%s\'...]\n' % (datetime.datetime.now().isoformat(), '\' \''.join (args)))

timer = time.monotonic ()

try:
  try:
    proc = subprocess.Popen (args, text = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, bufsize = 1)
  except FileNotFoundError as e:
    if os.name == "os2":
      # Popen somehow tries .EXE but not .CMD (although can run .CMD), fix this.
      args2 = args
      args2 [0] = args [0] + '.cmd'
      try:
        proc = subprocess.Popen (args2, text = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, bufsize = 1)
      except FileNotFoundError:
        raise e
    else:
      raise e
except Exception as e:
  writeStdErr ('ERROR: Failed to start command: %s\n' % str (e))
  exit (255)

q = Queue ()
Thread (target=reader, args = [proc.stdout, q]).start ()
Thread (target=reader, args = [proc.stderr, q]).start ()

for source, line in iter (q.get, None):
  if source == proc.stdout:
    writeStdOut (line)
  else:
    writeStdErr (line)

rc = proc.wait ()

for source, line in iter (q.get, None):
  if source == proc.stdout:
    writeStdOut (line)
  else:
    writeStdErr (line)

timer = time.monotonic () - timer

writeStdErr ('[%s - Ended with exit code %s, elapsed %s s]\n' % (datetime.datetime.now().isoformat(), rc, datetime.timedelta (seconds = timer)))

log.close ()

exit (rc)
