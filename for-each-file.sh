#!/bin/sh

[ "$#" -ge 2 ] ||
{
  echo "\
Perform a command on each matching file in the current folder, recursively.

Usage: $0 <mask> <command> [<args>]

<mask>    File mask (specify '*' for all files). 
<command> Command to execute.
<args>    Command arguments (defaults to a list of mathcing files).
          If not empty, each occurence of '{}' will be replaced with
          a matching file name with the relative path and <command>
          will be executed per one file at a time.
"
  exit 1
}

MASK="$1"; shift
CMD="$1"; shift

echo "\
Execute   : $CMD $@
On files  : $MASK
In folder : `pwd`
Except    : files in folders starting with '.'
"
echo "Press Y and ENTER to proceed or anything else to abort."
read ANSWER

if [ "$ANSWER" = "Y" ] ; then 
  if [ -z "$1" ] ; then
    find . -name "$MASK" -type f ! -path "*/.*/*" -exec "$CMD" "{}" \+
  else
    find . -name "$MASK" -type f ! -path "*/.*/*" -exec "$CMD" "$@" \;
  fi
fi
